<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Livewire\Admin\Settings\UpdateSetting;
use App\Http\Livewire\Admin\Users\ListUsers;
use Illuminate\Support\Facades\Route;



Route::get('dashboard', DashboardController::class)->name('dashboard');
Route::get('users', ListUsers::class)->name('users');
Route::get('settings', UpdateSetting::class)->name('settings');
