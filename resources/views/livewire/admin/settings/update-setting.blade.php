<div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">General Settings</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form wire:submit.prevent="updateSetting">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="siteName">Site Name</label>
                                    <input wire:model.defer="state.site_name" type="text" class="form-control"
                                        id="siteName" placeholder="Enter name">
                                </div>
                                <div class="form-group">
                                    <label for="siteEmail">Site Email</label>
                                    <input wire:model.defer="state.site_email" type="email" class="form-control"
                                        id="siteEmail" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="siteTitle">Site Title</label>
                                    <input wire:model.defer="state.site_title" type="text" class="form-control"
                                        id="siteTitle" placeholder="Enter site title">
                                </div>

                                <div class="form-group">
                                    <label for="footerText">Footer Text</label>
                                    <input wire:model.defer="state.footer_text" type="text" class="form-control"
                                        id="footerText" placeholder="Enter footer text">
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input wire:model.defer="state.sidebar_collapse" type="checkbox"
                                            class="custom-control-input" id="sidebarCollapse">
                                        <label class="custom-control-label" for="sidebarCollapse">Sidebar
                                            Collapse</label>
                                    </div>
                                </div>

                                {{-- <div class="form-check">
                                    <input wire:model.defer="state.sidebar_collapse" type="checkbox"
                                        class="form-check-input" id="sidebar_collapse">
                                    <label class="form-check-label" for="sidebar_collapse">Sitebar Collapse</label>
                                </div> --}}
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-1"></i>
                                    {{-- @if ($showEditModal)
                                        <span>Save Changes</span>
                                    @else
                                        <span>Save</span>
                                    @endif --}}

                                    Save Changes
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
</div>



@push('js')
    <script>
        $("#sidebarCollapse").on('change', function() {
            $('body').toggleClass('sidebar-collapse')
        })
    </script>
@endpush
